\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Methodology}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Objectives}{3}{section.1.2}
\contentsline {section}{\numberline {1.3}Report Structure}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Schedule}{4}{section.1.4}
\contentsline {chapter}{\numberline {2}Background Research}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Board Games}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}3D Noughts \& Crosses}{5}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Physical Properties}{5}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Rules}{6}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Strategy}{8}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Computer Representation}{9}{subsection.2.2.4}
\contentsline {section}{\numberline {2.3}Adversarial Search}{10}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Game Tree}{10}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Minimax}{11}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Robotic Paradigms}{12}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Hierarchical Paradigm}{12}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Reactive Paradigm}{13}{subsection.2.4.2}
\contentsline {section}{\numberline {2.5}Robot Operating System (ROS)}{13}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Installation}{13}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Architecture}{13}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}Topics \& Services}{13}{subsection.2.5.3}
\contentsline {section}{\numberline {2.6}Baxter Research Robot}{14}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Baxter Software Development Kit (SDK)}{15}{subsection.2.6.1}
\contentsline {section}{\numberline {2.7}Existing Projects}{16}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Worked Example Visual Servoing}{16}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Connect Four Demo}{16}{subsection.2.7.2}
\contentsline {subsection}{\numberline {2.7.3}Baxter Solves Rubiks Cube}{16}{subsection.2.7.3}
\contentsline {section}{\numberline {2.8}Software}{17}{section.2.8}
\contentsline {subsection}{\numberline {2.8.1}Programming Languages}{17}{subsection.2.8.1}
\contentsline {subsection}{\numberline {2.8.2}Libraries}{17}{subsection.2.8.2}
\contentsline {subsection}{\numberline {2.8.3}Version Control}{17}{subsection.2.8.3}
\contentsline {chapter}{\numberline {3}Development \& Analysis}{18}{chapter.3}
\contentsline {section}{\numberline {3.1}Hardware Setup}{18}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Environment}{18}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Grippers}{19}{subsection.3.1.2}
\contentsline {subsubsection}{Size}{19}{subsubsection*.2}
\contentsline {subsubsection}{Shape}{19}{subsubsection*.3}
\contentsline {subsection}{\numberline {3.1.3}Custom Board Game}{19}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Vision}{20}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Blob Detection}{21}{subsection.3.2.1}
\contentsline {subsubsection}{Colour Calibration}{22}{subsubsection*.4}
\contentsline {subsubsection}{Area Threshold}{22}{subsubsection*.5}
\contentsline {subsection}{\numberline {3.2.2}Contour Detection}{22}{subsection.3.2.2}
\contentsline {subsubsection}{Analysis}{24}{subsubsection*.6}
\contentsline {subsection}{\numberline {3.2.3}Background Subtraction}{24}{subsection.3.2.3}
\contentsline {subsubsection}{Analysis}{24}{subsubsection*.7}
\contentsline {subsection}{\numberline {3.2.4}Hough Circle Detection}{25}{subsection.3.2.4}
\contentsline {subsubsection}{Analysis}{27}{subsubsection*.8}
\contentsline {subsection}{\numberline {3.2.5}World Coordinate System Transformations}{27}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Camera Calibration}{28}{subsection.3.2.6}
\contentsline {subsection}{\numberline {3.2.7}Pixel Alignment}{29}{subsection.3.2.7}
\contentsline {subsubsection}{Offsets}{32}{subsubsection*.9}
\contentsline {subsubsection}{Stacking}{32}{subsubsection*.10}
\contentsline {section}{\numberline {3.3}Game Logic}{33}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}2D}{33}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}3D}{34}{subsection.3.3.2}
\contentsline {subsubsection}{Generating Winning Lines}{35}{subsubsection*.11}
\contentsline {section}{\numberline {3.4}Robot Control}{35}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Basic Movement}{36}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Sensors}{36}{subsection.3.4.2}
\contentsline {subsubsection}{Infra-Red Limitations}{36}{subsubsection*.12}
\contentsline {subsection}{\numberline {3.4.3}Inverse Kinematics}{38}{subsection.3.4.3}
\contentsline {subsubsection}{Joint Limitations}{38}{subsubsection*.13}
\contentsline {subsection}{\numberline {3.4.4}Head Display}{39}{subsection.3.4.4}
\contentsline {section}{\numberline {3.5}Playing Noughts \& Crosses}{39}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}How to run programs}{39}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Structure}{40}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Robot Class}{40}{subsection.3.5.3}
\contentsline {subsubsection}{Vision}{40}{subsubsection*.14}
\contentsline {subsubsection}{Game Logic}{41}{subsubsection*.15}
\contentsline {subsubsection}{Movement}{41}{subsubsection*.16}
\contentsline {subsection}{\numberline {3.5.4}Board Class}{42}{subsection.3.5.4}
\contentsline {subsubsection}{\texttt {checkWinner()}}{42}{subsubsection*.17}
\contentsline {subsubsection}{\texttt {isFull()}}{42}{subsubsection*.18}
\contentsline {subsubsection}{\texttt {getCopy()}}{42}{subsubsection*.19}
\contentsline {subsection}{\numberline {3.5.5}Space Class}{42}{subsection.3.5.5}
\contentsline {chapter}{\numberline {4}Evaluation}{44}{chapter.4}
\contentsline {subsubsection}{Light Conditions}{44}{subsubsection*.20}
\contentsline {section}{\numberline {4.1}Board Detection}{44}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Pixel Point Accuracy of Hough Circle Detection}{44}{subsection.4.1.1}
\contentsline {subsubsection}{Results}{45}{subsubsection*.21}
\contentsline {subsection}{\numberline {4.1.2}Pixel Point Convergence}{45}{subsection.4.1.2}
\contentsline {subsubsection}{Results}{45}{subsubsection*.22}
\contentsline {section}{\numberline {4.2}Object Detection}{47}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Pixel Point Accuracy of Contours}{47}{subsection.4.2.1}
\contentsline {subsubsection}{Results}{47}{subsubsection*.23}
\contentsline {section}{\numberline {4.3}Game Play}{49}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Vision Classifications}{49}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Game Logic}{50}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Code Evaluation}{50}{section.4.4}
\contentsline {chapter}{\numberline {5}Conclusion}{52}{chapter.5}
\contentsline {subsection}{\numberline {5.0.1}Time Plan}{52}{subsection.5.0.1}
\contentsline {section}{\numberline {5.1}Extensions}{52}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Depth Sensing}{53}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Error Handling}{53}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Game Variants}{54}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Artificial Intelligence}{54}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Audio \& Voice Commands}{54}{subsection.5.1.5}
\contentsline {subsection}{\numberline {5.1.6}Head Display Nod}{55}{subsection.5.1.6}
\contentsline {subsection}{\numberline {5.1.7}Other Games}{55}{subsection.5.1.7}
\contentsline {chapter}{References}{56}{subsection.5.1.7}
\contentsline {chapter}{Appendices}{58}{section*.25}
\contentsline {chapter}{\numberline {A}Personal Reflection}{59}{Appendix.a.A}
\contentsline {chapter}{\numberline {B}Ethical Issues Addressed}{61}{Appendix.a.B}
\contentsline {chapter}{\numberline {C}Code}{62}{Appendix.a.C}
\contentsline {section}{\numberline {C.1}Repository}{62}{section.a.C.1}
\contentsline {section}{\numberline {C.2}New3DtictacBaxter.py}{62}{section.a.C.2}
\contentsline {section}{\numberline {C.3}BoardClass3D.py}{85}{section.a.C.3}
\contentsline {chapter}{\numberline {D}Board Detection Times Under Varying Light Conditions}{90}{Appendix.a.D}
\contentsline {chapter}{\numberline {E}Video}{92}{Appendix.a.E}
