\chapter{Evaluation}
\label{chapter4}
This chapter evaluates the performance of the robot. The most important aspects of functionality that required evaluation were the computer vision features. Such features will be covered using both qualitative and quantitative measures. I deemed aspects of the robot's movements, such as the inverse kinematics solver and other Baxter SDK functions, unnecessary to evaluate. This was due to the fact that results from such evaluations would only give indication to the robot's performance as a unit, rather than of how well the code I have written to run on it executes.

\subsubsection{Light Conditions}
All tests were performed with varying light conditions; minimal, artificial and natural light. 

\begin{itemize}
	\item Minimal light featured all the lights in Baxter's workspace turned off and the blinds on the nearby windows closed.
	\item  Artificial light had the lights turned on, and with the blinds kept closed.
	\item  With natural light, all the lights were turned off so that the only light coming in was through the open blinds. 
\end{itemize}

\section{Board Detection}
The final product detected the board via the use of Hough Circle detection (Section~\ref{hough_circles}). The circles on the board were evenly spaced so that the detector could calculate the mid points of the grid spaces. 

\subsection{Pixel Point Accuracy of Hough Circle Detection}
\label{pixeltesthough}
This first test was to determine how accurate the calculated centre point of the board space was. The program `\texttt{hough\_eval.py}' was created to take the calculated centre point average of the board over the course of its runtime. It also provided a function to print the pixel coordinates of a mouse click on the image feed. By measuring a central point on the physical board with a ruler and placing a dot visible enough to click on in the image feed, I was able to print the exact pixel coordinates board's centre point. This value was used as a ground truth. 


\begin{table}
	\begin{center}
		\begin{tabular}{| c | c | c | c | p{5cm} |}
			\hline
			\textbf{Lighting} & \textbf{Average Pixel} (x,y) & \textbf{Ground Truth} (x,y) & \textbf{Difference} (x,y) \\ \hline
			Minimal & (467,293) & (470,244) & (3,-49) \\ \hline
			Artificial & (467,293) & (470,244) & (3,-49) \\ \hline
			Natural & (467,293) & (470,244) & (3,-49) \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Hough Circle pixel point accuracy test results.}
	\label{hough_results}
\end{table}

\subsubsection{Results}
The results of the test (Table~\ref{hough_results}) shows a consistent average pixel across all light conditions. These averages were recorded over 1000 frames. Hough Circle detector uses a grayscale image as an input so the consistency in the calculated averages was expected. As long as the circles contrast to the background they are drawn on, brightness has little effect on their detection. 
The ground truth value was recorded once and used for every light condition (where a new average was calculated each time). In this case, we can assume that the ground truth was slightly off centre and due to human error on my part. A repeat of the test with a better fixed ground truth returned a difference of \texttt{(2,-1)} across all lighting conditions. Such a small pixel difference does not affect the overall performance of Baxter.

\subsection{Pixel Point Convergence}
\label{pixelcon_exp}
The previous test featured the detection of the board from a stationary arm. This test was done to evaluate the speed of which Baxter could centre his arm above the board's central point from its start position, under varying light conditions. The test was performed by running the final version of the project deliverable and using a stop watch to time from when the `Locating Board...' image was displayed until the `\texttt{default.png}' face was displayed, which is an indication to the user that Baxter has successfully located the board. 




\subsubsection{Results}

Appendix~\ref{houghtimetable} shows the recorded times of 10 runs of the program under three separate lighting conditions. Table~\ref{houghvariance} shows the mean and standard deviation of the recorded times. All the times have been plotted onto bar charts for each lighting condition and feature a horizontal black line, representing the mean time taken in that condition. This data can be seen together in Figure~\ref{barlight} or individually in Appendix~\ref{houghappendix}. Several conclusions can be drawn from the results of this experiment. 



\begin{table}
	\begin{center}
		\begin{tabular}{| c | c | c | c | p{5cm} |}
			\hline
			\textbf{Lighting} & \textbf{Mean} & \textbf{Standard Deviation} \\ \hline
			Minimal & 13.201 & 34.4 \\ \hline
			Artificial & 24.616 & 7.2 \\ \hline
			Natural & 15.466 & 3.4 \\ 
			\hline
		\end{tabular}
	\end{center}
	\caption{Mean and standard deviation of the recorded times.}
	\label{houghvariance}
\end{table}

\begin{figure}
	\begin{center}
		\begin{tikzpicture}
		\begin{axis}[ybar, width=16cm, height=10cm,ylabel={Time (Seconds)}, ymax=100, xtick=\empty, xtick pos=left,
		ytick pos=left, legend style={
			legend columns=3,
			at={(xticklabel cs:0.5)},
			anchor=north, draw=none
		},legend style={font=\footnotesize},area legend]
		\addplot[draw=blue,fill=blue] table [x, y, col sep=comma] {data.csv};
		\addplot[draw=black!30!green,fill=black!30!green] table [x, y, col sep=comma] {data2.csv};
		\addplot[draw=red,fill=red] table [x, y, col sep=comma] {data3.csv};
		\legend{Minimal, Artificial, Natural}
		\draw[draw=blue] (axis cs:\pgfkeysvalueof{/pgfplots/xmin},13.201) -- (axis cs:\pgfkeysvalueof{/pgfplots/xmax},13.201);
		\draw[draw=black!30!green] (axis cs:\pgfkeysvalueof{/pgfplots/xmin},24.616) -- (axis cs:\pgfkeysvalueof{/pgfplots/xmax},24.616);
		\draw[draw=red] (axis cs:\pgfkeysvalueof{/pgfplots/xmin},15.466) -- (axis cs:\pgfkeysvalueof{/pgfplots/xmax},15.466);
		
		\end{axis}
		\end{tikzpicture}
	\end{center}
	\caption{Experiments arranged in ascending order by time taken. Means drawn as horizontal lines in corresponding colour.}
	\label{barlight}
\end{figure}



According to the mean times (represented by horizontal lines), Baxter located the board fastest in minimal lighting conditions, however the standard deviation of the recorded times in this test was much greater than in other conditions. The high deviation infers that the consistency of Baxter's performance suffers in low lighting conditions. It is possible to see several recorded times straying far from the mean time line. It is unclear whether the two longest time results are either anomalies or signs of Baxter's possible unreliability in these conditions.

The slowest mean time was recorded in artificial light. From Figure~\ref{barlight} and Table~\ref{houghvariance}, it is possible to see that this average is a reliable representation of Baxter's performance. The relatively low standard deviation shows consistency between the times.

Natural lighting conditions showed the most consistent out of all the others. The chart shows the even spread around the mean time. Having performed this experiment, it was concluded that natural light was the most suitable condition for Baxter when locating the game board. This is due to consistency in the recorded times and the low mean time, which was not far off from the fastest average. However, Baxter still performed adequately across all light conditions, indicating that there is no specific lighting requirement for the robot to run in.


\section{Object Detection}
\label{obj_exp}
Baxter is able to differentiate between the coloured pieces by using a blob detector. The output of this detection has contours extracted from it, which is then used to locate individual objects of the desired colour (Section~\ref{contour_detect}). 

\subsection{Pixel Point Accuracy of Contours}
A test similar to the one carried out in Section~\ref{pixeltesthough} was performed to evaluate the accuracy of the contour detector. In the main program, the robot aligns itself with the centre point of a bounding box drawn around the extracted contour of an object. The program `\texttt{eval\_cont.py}' takes an average of the calculated centre points of each object from a stationary point, with the initial setup show in Figure~\ref{contnocont}. Like the `\texttt{eval\_hough.py}' test, a ground truth value was set for each piece's centre point, based on the returned pixel coordinates of mouse clicks. The aim was to find out which lighting conditions proved most suitable for detecting playing pieces.

\begin{figure}[ht]
	\centering
	\includegraphics[width=10cm]{images/nocont.png}
	\caption[calibtest]{Contour accuracy test setup. Blocks 1 to 10 (left to right, top to bottom).}
	\label{contnocont}
\end{figure}

\begin{table}
	\begin{center}
		\begin{tabular}{| c | c | c | c | p{5cm} |}
			\hline
			\textbf{Block Number} & \textbf{Average Pixel} (x,y) & \textbf{Ground Truth} (x,y) & \textbf{Difference} (x,y) \\ \hline
			1 & (459,205) & (458,203) & (1,2) \\ \hline
			2 & (598,190) & (602,188) & (-4,2) \\ \hline
			3 & (461,314) & (463,315) & (-2,-1) \\\hline
			4 & (594,301) & (594,302) & (0,-1) \\ \hline
			5 & (466,426) & (467,426) & (-1,0) \\ \hline
			6 & (596,422) & (599,424) & (-3,-2) \\ \hline
			7 & (468,552) & (471,553) & (-3,-1) \\ \hline
			8 & (602,534) & (606,539) & (-4,-5) \\
			\hline
		\end{tabular}
	\end{center}
	\caption{Contour accuracy test results after 1000 frames under natural lighting conditions.}
	\label{cont_results}
\end{table}

\subsubsection{Results}
The results of the test (Table~\ref{cont_results}) under natural lighting conditions were both accurate and reliable. The offsets from the ground truth values were minimal and the reliability of the detection can be seen in Figure~\ref{contnatlight} with a stable set of contours.

\begin{figure}[ht]
	\centering
	\includegraphics[width=10cm]{images/natlight.png}
	\caption{Detected contours under natural light.}
	\label{contnatlight}
\end{figure}

The results for the test under minimal and artificial lighting conditions were not reliable enough to include in this evaluation. The number of detected contours fluctuated between frames, resulting in disruption amongst the averages of the centre points, for the program was unable to determine which centre points were tied to each block. From this test we are unable to make comparisons on the accuracy of the contour detector under varying light conditions, however by comparing the images of the detected contours, it still gives indication of its reliability. The artificial light condition image in Figure~\ref{contartlight} reveals that not all the blocks were detected, and those that were had imperfect contours drawn around them. Though the number of detected contours was relatively stable, it was less than the number required by the test so the centre points were not recorded. The number of detected contours under minimal light had the greatest instability. Figure~\ref{contminlight} shows a time lapse of the detected blocks and how the number of contours changed. As with the artificial light results, the centre points of the contours in this test were also not recorded to to this instability.


\begin{figure}[ht]
	\centering
	\includegraphics[width=10cm]{images/artlight.png}
	\caption{Detected contours under artificial light.}
	\label{contartlight}
\end{figure}


\begin{figure}[ht]
	\centering
	\begin{tabular}{@{}cccc@{}}
		\includegraphics[width=6cm]{images/darklight1.png} &
		\includegraphics[width=6cm]{images/darklight2.png} \\
		\includegraphics[width=6cm]{images/darklight3.png} &
		\includegraphics[width=6cm]{images/darklight4.png} 
	\end{tabular}
	\caption{Detected contours under minimal light.}
	\label{contminlight}
\end{figure}

In conclusion to this test, the order at which the contour detector had the greatest stability under varying light conditions is as follows:

\begin{enumerate}
	\item Natural Light
	\item Artificial Light
	\item Minimal Light
\end{enumerate}

Though natural light is the optimal condition, Baxter will still function correctly in all other tested conditions.

\section{Game Play}
The final iteration of the 3D Noughts \& Crosses program was run on Baxter 10 times under each lighting condition. It was at this point when the timed results of the experiment in Section~\ref{pixelcon_exp} were recorded. The purpose of evaluating full length run-throughs was to test how well the individual components synchronised together and to fully test the game AI.

\subsection{Vision Classifications}
\label{visclass_exp}
One observation made during the run-throughs was how well the robot classified pieces played by its opponent. The aspects of the robot evaluated in Section~\ref{obj_exp} were present during this but not observed. Of these tests Baxter correctly classified all objects in 100\% of run-throughs across all three varying light conditions. 

\iffalse93.3\% of run-throughs across all three varying light conditions. There were only two errors, in both instances the robot was unable to detect that a new piece had been played using the background subtraction method (Section~\ref{bg_sub}). This occurred for one piece during the artificial light tests and one piece during the natural light tests. The reason behind these errors was due to user error, on my part. After realising my error and rerunning the tests, the robot correctly classified 100\% of objects across all lighting conditions. \fi

\subsection{Game Logic}
The test from Section~\ref{visclass_exp} involved a lot of observance of the robot's game logic. Due to the rule based system, the outcome of each game was predictable. The more I played the game, the quicker I was able to beat the robot. The number of turns to win a game converged to five, leaving nine pieces on the board. The results of this evaluation corresponds to the prior researched carried out and that it does not work as a game. This is assuming that a game must present some level challenge and a chance for either side to win. However in Section~\ref{tictacstrat}, it mentions that the game is already `broken' as the first player can always force a win. 

The robot is able to play the game to a degree which is challenging enough for new players. Baxter beat all of the three other people I had test the system in their first game against it. In some cases, Baxter managed to win further games against them. The human players eventually worked out a strategy to win (not necessarily the quickest). It is possible to change the code slightly to allow the robot to play first, decreasing the chances of the human players to win. 

\iffalse
The results also indicate that the behaviour of the robot was not intelligent, for it follows a rule based system and does not show any evidence of adapting strategy based on game state. The focus of its behaviour was based entirely on the 49 generated winning lines and strategic value of particular spaces (such as the central space). I learnt from Newell and Simon's Tic-Tac-Toe program\cite{crowleysiegler}, that Baxter would of been a more formidable opponent if it was aware of the `fork' moves, which the entire strategy of Noughts \& Crosses is based on. A possible way of doing this would be to generate an array of all the possible `fork' moves, like the winning lines. This would require an interesting formula which would map every possible `fork' shape onto the cube of spaces. \fi



\section{Code Evaluation}
The program features a class-based structure (Section~\ref{classstructure}) which provides an easy to understand set of functions and properties. The functions after the constructor are split into sections; utility/setup (such as subscribers and boolean switches), game logic, tracking and movement. The data models for the board and individual spaces are stored in separate classes. The code was written in a style to allow modification of the game. 



