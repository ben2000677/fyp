\chapter{Background Research}
\label{chapter2}

This chapter documents the various aspects of prerequisite research before development began; Firstly, the nature of the board game including physical properties, rules, best strategies and any existing computer representations. Next, potential algorithmic approaches to playing the game via a set of rules, which is followed by robotic paradigms and their relevance. Finally, research into the robot itself and how one can operate it programmatically via ROS, including any previously done projects where the robot has been programmed to pick up and place objects. Further research had to be done during development in order to create alternative solutions to methods that did not work as planned. This has also been documented in this chapter.

\section{Board Games}
The board game I have chosen for the robot to play is 3D Noughts \& Crosses. I chose this over a more complex game with a complex set of rules, like chess, as I wanted the focus of the project to be robotics, rather than AI. Incidentally, the robotics aspect would be relatively easy to do with board games that feature playing pieces across a 2D plane. Selecting the three-dimensional version of Noughts \& Crosses (or Tic-Tac-Toe) was done so that more work could be put into developing the robot's motor functions and vision methods.



\section{3D Noughts \& Crosses}
Just like regular Noughts \& Crosses, the objective is to align 3 of your own playing pieces in a row, whilst preventing your opponent from doing the same. The exception is that, rather being played in a 3x3 grid, the game is played in a 3x3x3 cube, where the height of a piece being played, is determined by previously played pieces in that same position (similar to `Connect 4'). Figure~\ref{fig_two} shows a retail copy of the game. Looking at the image demonstrates that it is not possible to play a piece in a higher position without other pieces existing below it.

\begin{figure}[ht]
	
	\includegraphics[width=8.5cm]{images/fig2.png}
	\centering
	\caption{3D Noughts \& Crosses \cite{retailgameimg}.}
	
	\label{fig_two}
\end{figure}

\subsection{Physical Properties}
The retail version comes with 27 playing pieces. Each piece is either a 3D cross or a sphere (nought). Every piece features a hole running down the interior length. The `board' is essentially a small platform with 9 poles, evenly spaced to form a 3x3 grid (when looked at from above). The diameter of the poles are no more than a millimetre smaller than the diameter of the holes in the playing pieces. This ensures that when impaling a piece on the poles, it falls to the lowest possible position without any friction. This requires very finite and precise movement, which Baxter was not be able to perform, resulting in the need for a custom version of the board game (Section~\ref{custom}).

\subsection{Rules}
\label{rules}
The game is for 2 players only. One player is in possession of the noughts playing pieces, and the other player possesses the crosses. A starting player is randomly determined and play begins. On a player's turn, that player takes one of their playing pieces and places it on one of the 9 poles. Once the piece has fallen into position, the turn ends and the next player takes their turn. This action of placing a piece onto the board I will now refer to as `taking a position' and the player using crosses as the first player.

The end condition of the game is when either all 27 pieces have filled the playing area, or one player has taken three positions with  his pieces in a straight line. This line can be diagonal, vertical or horizontal across all axis. 

\begin{figure}[ht]
	
	\includegraphics[width=8cm]{images/fig28.png}
	\centering
	\caption{Possible winning lines \cite{4dtictac}.}
	
	\label{winningLines}
\end{figure}

There are a total of 49 winning lines in a 3x3x3 cube (see Figure~\ref{winningLines}). On his blog \cite{4dtictac}, Owen Elton talks about how tic-tac-toe could be played in a 3x3x3x3 hypercube. Calculating the winning lines on a 2D grid can be done by assigning `addresses' to each space in the grid, containing the space's row and column (such as in Figure~\ref{gridspaces}). \[address = (row, column)\]
\begin{figure}[ht]
	
	\includegraphics[width=7cm]{images/fig29.jpg}
	\centering
	\caption{Addresses of grid spaces \cite{4dtictac}.}
	
	\label{gridspaces}
\end{figure}

It is possible to identify whether a set of three distinct addresses form a winning line by looking at the differences in each element of the addresses. Take the set `\texttt{(a1,a2,a3)}', notice how the first element of each address is identical and is therefore a winning line. For horizontal and vertical winning lines, the alphabetic or numeric properties of the cells' addresses are all the same. For the diagonals, the properties of the cells' addresses must be all different, however, a caveat must be added that if both alphabetic and numeric properties are all different then the set must contain the central cell \texttt{(b2)} to qualify as a winning line \cite{4dtictac}.

This article helped me when applying these rules to find the 49 winning lines of a 3-dimensional cube, in which each address comprised of 3 elements instead of 2. Section~\ref{board_class} describes in more detail how I applied this information to generate the winning lines programmatically. 


\subsection{Strategy}
\label{tictacstrat}
The original game of Noughts \& Crosses, has a very simple strategy in which if both players play optimally, the game will result in a draw.
Allen Newell and Herbert Simon created a Tic-Tac-Toe program in 1972 \cite{crowleysiegler} which followed the following set of rules:
\begin{enumerate}
	\item \textbf{Win:} If the player has two pieces in a row, they can place a third piece to get three in a row.
	\item \textbf{Block:} If the opponent has two pieces in a row, the player must play the third piece themselves to block the opponent.
	\item \textbf{Fork:} Create an opportunity where the player has two possible lines to win.
	\item \textbf{Option 1:} The player should create a `Block' situation with two pieces in a row to force the opponent into defending, as long as it doesn't result in them creating a `Fork' situation. 
	\item \textbf{Option 2:} If there is a configuration where the opponent can create a `Fork' situation, then the player should block that fork.
	\item \textbf{Center:} A player marks the centre. 
	\item \textbf{Opposite corner:} If the opponent plays a piece in the corner, the player plays a piece in the opposite corner.
	\item \textbf{Empty corner:} The player plays in a corner space.
	\item \textbf{Empty side:} The player plays in a side space.
\end{enumerate}

This rule set was very useful when developing the 2D version of the game to play. In Section~\ref{game_logic} I use a similar, but simplified version of these rules for the robot to follow. 

For the opening move, there are 9 places the first player can play into. However, this can be logically simplified to 3 places, as all the corner spaces are strategically equivalent and all the side spaces are strategically equivalent. This is only for the first turn. Of these three moves, the one with the most options to win for the first player is the corner \cite{hexafl}. This is because, out of the 8 remaining spaces for the second player, they can only avoid a loss by taking the centre space. If the first player had taken the centre, the second player avoids a loss by taking a corner, of which there are 4. This information was not used however, for the implementation of the game logic in the robot always had the human player acting first. This decision was made so that certain states of the board could be forced for testing purposes.

The 3D variant of the game has a different strategy. When played on a 3x3x3 cube, the game is easily winnable once a player has a piece in the central position \cite{tictacwiki}. With the retail version of the game, this can occur when one player takes the centre position on the lowest level of the board, but also having another piece next to it, forcing the other player to block by taking a position on the lowest level. Figure~\ref{fig_three} shows how the second player is unable to take the central position without losing to the first player on the lowest level. It is even possible to force a win from a much earlier point in the game by having the first player take the central space on the lowest level, and then taking the space above it on their next turn. 

\begin{figure}[ht]
	\centering
	\begin{tabular}{@{}cccc@{}}
	\includegraphics[width=3cm]{images/fig3.png} &
	\includegraphics[width=3cm]{images/fig4.png}
	\end{tabular}
	\caption[Tic Tac Toe Example]{Two instances where the second player is forced to block.}
	\label{fig_three}
\end{figure}

The result of this is the game devolves into the winner being the player who takes the first turn. This discovery made the incorporation of the game logic into the robot very easy. It shows that the game is `solvable', which means there is always an optimum move to take. As the focus of the project is not to develop a challenging AI but rather have the robot interact in physical space with accuracy, the game did not need to be changed in light of this.

\subsection{Computer Representation}
There are many online games of 3D Noughts \& Crosses. However, due to the flawed nature of the 3x3x3 variant, the majority of them are played on a 4x4x4 board, which is much less predictable. I also discovered that it is possible to play the game without the gravity factor of the retail version. Unfortunately this allows the first player to take the central position at the start of the game, further simplifying the game's strategy.  

\begin{figure}[ht]
	\centering
	\begin{tabular}{@{}cccc@{}}
		\includegraphics[width=5cm]{images/fig5.png} &
		\includegraphics[width=5cm]{images/fig6.png}
	\end{tabular}
	\caption[3D Tic Tac Toe]{Sequence of moves until game end in 3D \cite{3dtictacexamplepic}.}
	\label{fig_four}
\end{figure}

Using screenshots from one of these games, Figure~\ref{fig_four} shows the next logical moves from the example in  Figure~\ref{fig_three}, once the first player (orange) takes the central position. The second image shows the second player (blue) is forced to block by taking the top centre position, allowing the first player to take a move leading them to win the game. Whatever move the second player makes in this state of the game, they cannot prevent the first from winning.

Switching to the 4x4x4 variant of the game is a possible extension I discuss in Section~\ref{variants}. Developing the AI for this will be more challenging though, and like the 3x3x3 variant the first player can force a win \cite{oPatashnik}.

\section{Adversarial Search}

There are a number of algorithms that can be used to solve the best strategy for particular games. This section explores the algorithms which were appropriate for the implementation of the robot's game logic. 

Games usually fall under adversarial search problems. Noughts \& Crosses is a common example used in game theory, it can be classed as a deterministic, turn-taking, two-player, zero-sum game of perfect information \cite{russelnorvig}. The game is deterministic because there is no elements of chance, such as dice or decks of cards. Such games are much easier to solve than games of chance. Perfect information refers to all elements of the game being visible at all times, whereas imperfect information in games is featured as hidden details, such as an opponent's hand of cards in poker. Each move made by a player (with the aim to win the game) adversely affects the opponent's chances of winning. This is known as a zero-sum game, and as the players are each trying to ensure they win whilst simultaneously ensuring their opponent loses, it makes the situation adversarial. 

\subsection{Game Tree}
\label{gametree}
A game tree is a directed graph which shows all the possible states of a game. A full tree consists of the initial state of the game as the root node, and branches down to all the possible variations of terminal states at the leaf nodes. Each level of the tree represents all the moves which can be made by a player for each turn in the game, and this alternates between the players every level. In the case of a 3x3 grid, the first player can make one of nine moves from the initial state therefore nine states branch out from the root node. The next level contains all the possible states of the game after the second player makes a move in response to the first, and so on. Figure~\ref{gametreefig} shows a more streamlined game tree for Noughts \& Crosses, showing the 3 possible opening moves (mentioned in the previous section). There are fewer than 9! = 362,880 terminal nodes \cite{russelnorvig} in the full tree of a standard game of Noughts \& Crosses. This relatively small number means that it is entirely plausible to generate a game tree for the 3D variant of Noughts \& Crosses, and implement an algorithmic approach to the robot's artificial intelligence when playing it against a human opponent. 


\begin{figure}[ht]
	
	\includegraphics[width=9cm]{images/fig30.png}
	\centering
	\caption{Game tree, showing states of playing in a corner, side or in the centre \cite{treewiki}.}
	
	\label{gametreefig}
\end{figure}

\subsection{Minimax}
\label{minimax}
The minimax algorithm can be used to search a game tree to calculate the best possible solution at each given state. As mentioned in Section~\ref{gametree}, each level of the Noughts \& Crosses tree alternates between the players. Minimax goes through these levels maximising the chances of winning for one player and minimising for the other. The algorithm can be described by naming the players `Min' and `Max'. Max will go first and both players will keep playing until the end of the game. This creates a game tree similar to Figure~\ref{gametreefig}. When the terminal states are reached, scores (known as utility values) are placed at the leaf nodes, a high value for a terminal state where Max won and a low value for where Min won. These utility values are calculated at all the child nodes in the tree. Figure~\ref{minimaxfig} shows an example of how this is done. Starting at the parents of the leaf nodes, Max will choose the highest value from each of the child nodes (In the left-most branch, Max chooses a 3 because it is higher than the 2). The chosen value is labelled at the parent node, and Min now must choose the lowest value from these nodes to label their parent nodes (in the left-most branch, Min has also picked the 3 because it is lower than the 9). Going up through the tree, the initial node is reached, which is the 3 in this example. This path is how the current player decides which move to make. The move being chosen will follow the path that benefits them (Max) the most whilst hindering the other player (Min) the most.

\begin{figure}[ht]
	
	\includegraphics[width=12cm]{images/fig31.jpg}
	\centering
	\caption{Minimax Example \cite{minimaximg}.}
	
	\label{minimaxfig}
\end{figure}

The algorithm performs a complete depth-first exploration of the tree, using a simple recursive computation of the minimax values for each successor state \cite{russelnorvig}. At each node (or state) of the tree, depending on whether the depth of that node is either Max's or Min's turn, the algorithm will choose the highest or lowest utility value of its child nodes, respectively. However, in order to work out the utility value of the child nodes, the minimax computation must be run on their respective child nodes. This recursion continues to the leaf nodes (or terminal states) of the tree, and then the minimax values are passed up through the tree where they were required, resulting in a tree like Figure~\ref{minimaxfig}. Once all the child nodes have utility values assigned to them, the decision aspect mentioned in the previous paragraph can be carried out.


\section{Robotic Paradigms}
\label{paradigm}
A paradigm describes the approach to a problem whilst implying a possible structure for its potential solution \cite{airobin}. A robot paradigm makes use of three primitives which can be used to categorize functions within the robot.
\begin{itemize}
	\item \textbf{Sense} - Handle raw information from sensing/detecting aspects of the robot's environment. Examples include camera feeds and range sensors.
	
	\item \textbf{Plan} - Take information from either sensory input or innate knowledge, then compute appropriate actions to reach a goal state.
	
	\item \textbf{Act} - Perform actions which result in the manipulation of the environment. This includes a change in location of the robot itself via movement.
\end{itemize}

A given paradigm describes the relationship between these three primitives \cite{airobin}.

\subsection{Hierarchical Paradigm}
This paradigm features a top-down approach. The robot first takes sensory data of the surrounding environment until it has enough data to move onto the next stage. From the sense stage, it takes the data and formulates a best possible action to perform in the plan stage. Finally, this action is physically performed by the robot's motors in the act stage. This continually loops until a goal state has been reached. 
The downside of this paradigm is that a lot of time is lost during the plan stage. In a dynamic environment this will come across as an unresponsive robot, since the computations of the plan stage will not take into account that aspects of the environment can change during this time. However, due to the environment featured in my project remaining static for the duration of the game, this is the paradigm that I adopted.

\subsection{Reactive Paradigm}
The reactive paradigm comprises of multiple SENSE-ACT couplings, eliminating the need for a plan stage. This is the solution to unresponsive robots in dynamic environments for each type of sensory input directly translates to a particular action. 
The downside of this paradigm is that for complicated sensory inputs, there must be an increase in the number of SENSE-ACT couplings for each possible environmental reaction. As the number of these environmental factors grow (sense), so do the reaction functions (act), and the difficulty arises in managing them.


\section{Robot Operating System (ROS)}
\label{rossec}
ROS is a free, open-source framework for writing robotics software. Its flexibility comes from a wide selection of tools, libraries and conventions. The Baxter research robot used in this project runs on ROS, so becoming familiar with it was a prerequisite step. 

\subsection{Installation}
Each distribution of ROS is targeted at running in specific releases of the Ubuntu distribution of Linux. ROS Indigo is the distribution that the robot is currently running, which is primarily targeted at the Ubuntu 14.04 LTS release \cite{indigo}. After installing VirtualBox VM software on my machine, I installed Ubuntu 14.04 with ROS Indigo in order to familiarize myself with the ROS framework and follow the tutorials found on the wiki.

\subsection{Architecture}
ROS works very much like a file system. Most of the functionality comes in the form of terminal commands; `\texttt{roscd}' to change directory, `\texttt{rosrun}' to run a program. Every project created in this file system is stored in the form of a package. The package contains all software and data, as well as header files to import necessary dependencies.  Once a package is ready to be built, the `\texttt{rosmake}' command is entered to compile all the packages in the file system alongside all the required dependencies. 

\subsection{Topics \& Services}
A robot is usually a series of connected nodes and sensors. The nodes tend to be motors which allow the robot to interact with the physical environment and the sensors are a form of input, containing information about the robot's surroundings. ROS is able to interact with these aspects via the medium of \textit{Topics} and \textit{Services}. `\texttt{rostopic list}' will list all the available topics. Topics are the buses in which the nodes in a robot exchange messages. An example of a topic could be the rotational state of a particular joint in the arm of a robot. 

Services are subscribed to in order to utilize information which is feeding into the robot. For example, to output the image feed from a camera on the robot, the software subscribes to that camera's service. `\texttt{rosservice list}' lists all the available services. 

\section{Baxter Research Robot}

\begin{figure}[ht]
	\centering
	\includegraphics[width=10cm]{images/fig7.jpg}
	\caption[Baxter]{Baxter Research Robot by Rethink Robotics \cite{baxterimg}.}
	\label{fig_seven}
\end{figure}

Baxter is an industrial robot built by Rethink Robotics and introduced in September 2012 \cite{baxterwiki}.  Baxter is a 3-foot tall (without pedestal; 5'10" - 6'3" with pedestal), two-armed robot with a screen that can be used as an animated face. It weighs 165 lbs without the pedestal and 306 lbs with the pedestal \cite{baxterspecs}. Throughout the project I worked with the robot whilst it was mounted on its pedestal. Every joint in each arm features a servo motor, whose angles are adjusted in order to achieve movement. Holding the `cuff' off an arm puts the robot into `Zero Force Gravity Compensation mode', allowing free manipulation of the limb by physical guidance. The robot has 3 cameras, one in the wrist of each arm and one built into the face display. There are also infra-red sensors on the wrists which calculate distance. The robot comes with several interchangeable grippers for the arms each varying in width and length. There is a selection of holes that the grippers can be screwed into in order to adjust the overall distance between them (see Figure~\ref{fig_eight}). The grippers are opened and closed by a servo motor as well. I will be referring to the Baxter Research Robot as `Baxter' for the rest of this report.  


\begin{figure}[ht]
	\centering
	\includegraphics[width=10cm]{images/fig8.png}
	\caption[Baxter Arm]{Camera, infra-red sensor and gripper adjustment \cite{armimg}.}
	\label{fig_eight}
\end{figure}

\subsection{Baxter Software Development Kit (SDK)}
The Baxter SDK provides an interface to allow custom applications to be run on the Baxter. The main interface of the Baxter SDK is via ROS Topics and Services, which is why learning ROS was a necessity. The SDK comes with libraries to control Baxter from programs. The `Hello Baxter!' tutorial on Rethink Robotics wiki shows how to store the angles of the motors in Baxter's arm, manipulate them, and then update the arm with the new angles. The SDK also includes example scripts to run on Baxter, including the important `\texttt{tuck\_arms.py}' which I used at the start and end of each development session to store the robot. There is another example which maps every motor in the robot to the keyboard. This is a very impractical way of controlling Baxter for the movements are not precise. However, I found the control of the grippers in this script useful in testing different gripper sizes on varying objects.


\section{Existing Projects}

It was important to find out whether my project had been done before. Finding existing projects prove useful in seeing where others have succeeded or failed in the research you are planning to do. In searching for projects identical to my own, I discovered that this project is entirely unique. However, projects with similar goals and solutions were found.

\subsection{Worked Example Visual Servoing}
This example \cite{golfex} is targeted at 1st-2nd year undergraduates on courses similar to my own. The aim of the project is to have Baxter pick up golf balls, and place them into slots in an egg tray. The example uses Visual Servoing for workspace relationship and calibration, Visual Object Recognition and target selection. It also makes use of inverse kinematics. This project helped me greatly during the development phase of my project. Section~\ref{inverse_kin}, Section~\ref{WCST}, and Section~\ref{pixel_align} detail development work which was influenced by this example, specifics can been seen in each section. I will be referring to this project as the `Golf Example' from now on. 

\subsection{Connect Four Demo}
The Connect Four Demo \cite{connect4} features Baxter playing the original game of Connect 4, either against a human opponent or itself. There is a requirement for a custom game piece feeder which allows Baxter to pick up the game pieces with ease. The board detection aspect has the user click and drag over the image coming from the camera feed, to highlight the board. An HSV blob detector (like the one I use in Section~\ref{blob_detect}) is used to identify which colour game pieces have been played.  

\subsection{Baxter Solves Rubiks Cube}
This project \cite{rubiks} has Baxter solving the Rubik's cube puzzle. The stages of runtime are separated into detection, verification, solving and manipulation. Detecting the current state of the cube is done with the use of a flatbed scanner. Baxter does not utilise any of its detection hardware for this project. The location of the scanner is taught by using Baxter's `Zero Force Gravity Compensation mode'. It then scans each of the six sides of the cube and verifies that it has scanned them correctly. The Kociemba algorithm is then run to solve the cube and return a list of the required manipulations. Baxter then performs these manipulations, in a manner similar to that of a human. This project did not feature any aspects that crossed over with my objectives, therefore I did not refer to it further during development.

\section{Software}

\subsection{Programming Languages}
The available programming languages for ROS are Python and C++. I selected to use Python, due to its simplicity and the fact that the Baxter SDK examples were also in Python. The performance advantage gained from using C++ is not necessary for the purposes of this project. 

\subsection{Libraries}
Apart from the Baxter SDK, another library I made use of was OpenCV. As the name implies, it is an open source set of programming functions aimed at doing computer vision in both images and video frames. All of the computer vision aspects of the project made use of this library. 


\subsection{Version Control}
A repository was set up on my Bitbucket account for all the source code (including this report) developed during the project. I used git commands to pull and update code from the repository when changes were made. This method allowed me to work on the project from multiple places as well as provided a backup in case of any hard disk failures. The link to the repository has been included in Appendix~\ref{codeappendix}.




