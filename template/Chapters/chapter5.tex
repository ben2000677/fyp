\chapter{Conclusion}
\label{chapter5}

The final system meets all but one of the objectives from Section~\ref{objectives}. This outstanding objective was to implement a level of intelligence into the robot so that it develops a strategy for playing the game over time. The goal proved to be too ambitious given the scope of the project. Therefore, I spent more time ensuring the other objectives were met and the resulting functionality worked robustly.

\begin{itemize}
	\item Baxter successfully locates and picks up playing pieces and places them into specific areas of the game board with the ability of stacking them up to three levels.
	\item Baxter is able to recognise moves made by the human player and store their corresponding locations.
	\item Baxter can respond to the human player's moves strategically and by following the game rules. The game has also been implemented into a software-only environment. 
\end{itemize} 


\subsection{Time Plan}
Figure~\ref{newtime} shows a revised version of the Gantt chart from Section~\ref{Schedule} with the actual times it took to complete each objective. The overall time of the project was extended by three weeks due to a change in the deadline. This change was at a request from me to the university regarding family related problems at the time, incurring a set back in progress. The obvious change in the time plan is that the first objective took more time than expected. This is because the objective had many prerequisite steps, such as locating the board and calculating the grid positions. The second objective was easy to implement once a certain number of these steps were completed. Objective four (implementing game logic in a software environment) took a bit longer than planned because it was worked on at separate points; the first few days of the segment a 2D version of the game was developed, and the last few days the code was upgraded to the 3D version. A similar occurrence happened with objective three; the background subtraction was implemented in the first five days of the segment, and development on the Hough Circle method started when the 3D version of the game logic was implemented. The time it took to complete this objective still managed to stay true to the estimate.

\begin{figure}[ht]
	\centering
	\includegraphics[width=16cm]{images/newtimeline.png}
	\caption[Gantt Chart2]{Actual time plan for each objective.}
	\label{newtime}
\end{figure}

\section{Extensions}
This section discuses the ways in which the project can be extended for increased functionality and different uses. The number of possible extensions is fairly limited by Baxter not being easily modifiable as a unit. 

\subsection{Depth Sensing}
One way the robot can have its hardware modified is by the inclusion of a Kinect sensor (by Microsoft, originally built for the Xbox 360). The Kinect features an RGB camera and a depth sensor \cite{kinectref}, allowing it to perceive environments in 3D. Such a devise can be used to upgrade the tracking aspects of the project. 

Attaching a Kinect to the front of the robot will negate the need for pixel alignment (Section~\ref{pixel_align}) and World Coordinate System transformations (Section~\ref{WCST}), for the world coordinates of the block centre points can be calculated from two different perspectives also giving a much greater accuracy. 

By attaching the sensor to the `cuff' of the robot (facing down towards the board), the heights of objects played on the board can be detected with the depth sensor. Currently the background subtraction (Section~\ref{bg_sub}) and Hough Circles (Section~\ref{hough_circles}) methods recognise moves made by the player. Having the Kinect sensor implemented would negate the requirement to have circles drawn on one side of each of the human player's pieces. 


\subsection{Error Handling}
For the purposes of time, I did not put much focus into handling any errors that the user may cause in running the demo. 

If the human player played a piece in between two spaces, Baxter will register it as being played in the first space which has the smallest distance from the piece's centroid. If Baxter were to play a piece in the empty of those two spaces, it would collide with the piece played by the human player for it covers an unexpected area. A fix for this would be to calculate the offset of the human's piece from the centre of space it is registered as being played in. Baxter could then shift all the playing positions of the other grid spaces by this offset, so as not to collide with any pieces. An alternative fix would be to have Baxter pick up the human's piece and place it correctly in the registered space. On the assumption that the human player will always place their pieces in the designated squares of the board, I did not implement either of these.

\subsection{Game Variants}
\label{variants}
There are many variants of Noughts \& Crosses. `Qubic' features a 4x4x4 board and presents a much bigger challenge to both the player and someone developing an AI to play it (Though also solvable \cite{oPatashnik}, like the 3x3x3 variant). The code in this project has been developed to allow modifications to work with larger game boards. Playing the regular 2D version of the game is also very possible, as it was a prior iteration before the final. Other variants such as `Connect Four' are worth a mention but are not as easily adapted to as official variants of Noughts \& Crosses.

\subsection{Artificial Intelligence}
Due to the nature of the board game, there came a small amount of challenge when playing it over time. The focus of its behaviour was based on the 49 generated winning lines and strategic value of particular spaces (such as the central space). The system can be made more intelligent by a number of methods. I learnt from Newell and Simon's Tic-Tac-Toe program\cite{crowleysiegler}, that Baxter could be a more formidable opponent if it was aware of the `fork' moves, which the entire strategy of Noughts \& Crosses is based on. A possible way of doing this would be to generate an array of all the possible `fork' moves, like the winning lines.
  
Adaptive strategy based on game state could be implemented with Minimax. Section~\ref{minimax} discusses Minimax and how the algorithm works out a best possible move at each state. Implementing this into the robot's game logic would provide even more challenging game play. 

It is possible to program an unbeatable strategy for the robot if it were to play first. This would not provide a fair game to the human player. Implementing an AI `level' (such as easy, medium and hard, where `hard' utilizes the unbeatable) could simulate more realistic gameplay, with a further add-on randomizing the starting player.


\subsection{Audio \& Voice Commands}
The current method of Baxter recognising a move  made by a player, is to continually loop through detection methods between timers until a change is detected. As this is a turn-based game, this real-time aspect may hinder a user's strategy. A simple solution to this is to implement a simple voice command, where the human player can say a phrase (such as `turn!') to indicate to the robot that it should start its detection methods to find where the human has made their move. 

The acknowledgement method of a move being successfully detected is currently to publish the resulting image to Baxter's head display. Using a text-to-speech synthesiser, this method can be replaced with the robot `speaking' to indicate to the human player that their turn has been detected.

\subsection{Head Display Nod}
Another way of acknowledging a move made by the human player is to manipulate the position of the head display to nod. Though equally trivial as the `speaking' method of the previous section, it provides further exploration of Baxter's available functions.

\subsection{Other Games}
Aside from the Noughts \& Crosses variants, there are other games this project can be extended to work for. The functions of the `\texttt{robot}' Class in the code (described in Section~\ref{robotclass}), provide movements which are universal to most board games. Along with the detection functions, most of the code can be applied to play different games, the only requirement being a change in the game logic and data models. One possible game could be Draughts (or Checkers).   
