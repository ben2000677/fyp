# README #

### What is this repository for? ###

Source code for my Final Year Project 'Playing 3D Noughts & Crosses with Baxter, a humanoid robot'


### How do I get set up? ###

* Follow [this](http://sdk.rethinkrobotics.com/wiki/Workstation_Setup) tutorial on setting up a workstation to use with Baxter 
* Clone this repository into your ROS workspace ('.../ros_ws/src/')
* Each program can be run with 'rosrun fyp <program name>.py'
* Ensure you run 'golf_setup.py' to update the setup.dat file with table height.
* Also calibrate colours with 'Object_finder.py'
* The final program is 'New3DtictacBaxter.py', run this for the full demo.


### Who do I talk to? ###

* Ben Hirsh - ben2000677
* ben.hirsh@hotmail.com